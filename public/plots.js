




// 2D Chart.js
var ctx = document.getElementById("myChart");
var myChart = new Chart(ctx, {
          type: 'line',
          data: {
              labels: range(400,750),
              datasets: []
          },
          options: {
            legend: {
              display: false
            },
            responsive: false
          }
      });
var pca_ctx = document.getElementById("pcaChart");
var pcaChart = new Chart(pca_ctx, {
          type: 'bubble',
          data: {
              labels: [],
              datasets: []
          },
          options: {
            legend: {
              display: false
            },
            responsive: false,
            elements: {
                points: {
                    borderWidth: 1,
                    borderColor: 'rgb(0, 0, 0)'
                }
            }
          }
      });



function plotPCA(allSpecs, allSpecsClassIDs){
  console.log("Plotting PCA!");
   var XR = pcaReduce(pca(allSpecs) , 2); // 2 loadings
   var XR3 = pcaReduce(pca(allSpecs), 3); // 3 loadings

   var data = [];
   for(var i = 0; i < allSpecs.length; i++){
      var bubblePoint = pcaProject(allSpecs[i], XR);
      var d = {
        label: allSpecsClassIDs[i],
        data: [
            {
                x: bubblePoint[0],
                y: bubblePoint[1],
                r: 5
            }
        ],
        backgroundColor:  frame.$data.classColors[allSpecsClassIDs[i]],
        hoverBackgroundColor: "#FF6384",
      };

      var bubblePoint3D = pcaProject(allSpecs[i], XR3);
   
      var trace = {
        x: [bubblePoint3D[0]], y:[bubblePoint3D[1]], z: [bubblePoint3D[2]],
        mode: 'markers',
        name: allSpecsClassIDs[i],
        marker: {
          color: frame.$data.classColors[allSpecsClassIDs[i]],
          size: 5,
          line: {
          color: '#fff',
          width: 0.5},
          opacity: 0.8},
        type: 'scatter3d'
      };
      data.push(trace);



      // knn injection
      var K = {
        x: bubblePoint[0],
        y: bubblePoint[1],
        classID: allSpecsClassIDs[i],
        classColor: frame.$data.classColors[allSpecsClassIDs[i]]
      }       

     

    //  
      pcaChart.data.datasets.push(d);
      pcaChart.update();
    }// end for loop

    var layout = {margin: {
      l: 0,
      r: 0,
      b: 0,
      t: 0
    }};
    Plotly.newPlot('myDiv', data, layout);

}






