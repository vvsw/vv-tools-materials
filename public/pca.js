function pca(X) {
    /*
        Return matrix of all principle components as column vectors
    */        
    var m = X.length;
    var sigma = numeric.div(numeric.dot(numeric.transpose(X), X), m);
    return numeric.svd(sigma).U;
}

function pcaReduce(U, k) {
    /*
        Return matrix of k first principle components as column vectors            
    */                
    return U.map(function(row) {
        return row.slice(0, k)
    });
}

function pcaProject(X, Ureduce) {
    /*
        Project matrix X onto reduced principle components matrix
    */
    return numeric.dot(X, Ureduce);
}

function pcaRecover(Z, Ureduce) {
    /*
        Recover matrix from projection onto reduced principle components
    */
    return numeric.dot(Z, numeric.transpose(Ureduce));
}
         
         /*        
window.onload = function() {
    var x, y, X = [];
    
    var noise = function() {return Math.random() * 0.02 - 0.01};
    
    // Create random dataset with slope of 0.357 and noise
    for (var i = 0; i < 1000; i++) {
        x = Math.random() * 2 - 1;
        y = x * 0.357;
        X.push([x + noise(), y + noise()]);
    }
    
    // Get principle components
    var U = pca(X);
    
    // Print slope of first principle component
    document.write(Math.abs(U[0][1] / U[0][0]).toFixed(3));
};
*/