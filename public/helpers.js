function fetchClassSpectrums(classID, continuation){

    return new Promise(function(resolve, reject){
      var SpectrumRef = new Firebase("https://verivina.firebaseio.com/spec_db");
      classRef = SpectrumRef.child(classID);
      classRef.on('value', function(snapshot){
        var class_obj = snapshot.val();
        if(frame.$data.classSpectrums[classID] == undefined) { frame.$data.classSpectrums[classID] = []};
        for(key in class_obj.measurements){
          var single_scan = class_obj.measurements[key];
          var spectrum_data = formatSpectrum(single_scan.spectrum, false);
          frame.$data.classSpectrums[classID].push(spectrum_data);
        }
        frame.$data.fetchedClasses[classID] = true;
        continuation(classID);
        resolve(true);
      });
  });

}


function normalizeSpectrum(spec){
    var returnSpectrum = spec;
    var maxPower = Math.max.apply(Math,returnSpectrum);
    var normalized = [];
    for(var i=0; i < returnSpectrum.length; i++){
        normalized[i] = returnSpectrum[i]/maxPower;
    }
    return normalized;
  }

function whiteCorrect(spec, single_white){
  var correctedSpec = [];
  for(var i = 0; i < single_white.length; i++){
    correctedSpec[i] = spec[i]/single_white[i];
  }
  return correctedSpec;
}

function formatSpectrum(spec, normalizeBool){
  var returnSpectrum = [];
  for( var i = 0; i < spec.length; i++){
    returnSpectrum.push(parseFloat(spec[i][1]));
  }
  if(normalizeBool == true){
    return normalizeSpectrum(returnSpectrum);
  }
  else{
    return returnSpectrum;
  }
}

function range(n1, n2){
  returnArray = [];
  for(var i= n1; i < n2; i++){
    // do intervals of 20, fill rest with ' ' empty
    if(i % 20 == 0) {returnArray.push(i);}
    else {returnArray.push('');}
  }
  return returnArray;
}

function getRandomColor() {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++ ) {
      color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

// archives specDB and cleans 
function archiveSpecDB(){
  console.log("Archiving spec_db...!");
  var destination = "archive";
  var oldRef = new Firebase("https://verivina.firebaseio.com/spec_db");

  // Attach an asynchronous callback to read the data at our posts reference
  oldRef.on("value", function(snapshot) {
    var val = snapshot.val();
    var newRef = new Firebase("https://verivina.firebaseio.com/archive_00");
    newRef.set(val); // copy oldRef to newRef
    console.log("Archiving done!");    //oldRef.set({}); // clear the oldRef
    console.log(snapshot.val());
  }, function (errorObject) {
    console.log("The read failed: " + errorObject.code);
  });
}


