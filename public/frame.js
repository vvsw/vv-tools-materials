
var OB;
// INIT VUE.JS
var frame = new Vue({
    el: '#mainDiv',
    data: {
      titleShow: false,
      message: 'What the fuck is this',
      classList: [],
      selectedClass: {},
      classSpectrums: {},
      fetchedClasses: {},
      checkedClasses: {},
      classColors: {},
      whiteCorrectionEnabled: false,
      normalizationEnabled: true,
      whiteCorrectionID: "",
      knearMachine: {},
      allSpecs: [],
      allSpecsClassIDs: [],
      classifierGuess: "N/A"


    },
    init:
      function (){
        // Only fetch the list of classes, not their data
        var SpectrumRef = new Firebase("https://verivina.firebaseio.com/spec_db_classes");
        SpectrumRef.on("child_added", function(data, prevChildKey){
          var class_obj = data.val();
          var classID = data.key(); // ie: LEDsample1
          frame.$data.classList.push(classID);
          frame.$data.fetchedClasses[classID] = false;
          frame.$data.classColors[classID] = getRandomColor()
        })
      }
    ,
    methods: {
      InitClassifier:
      function(event){ // we want to fetch all data for selected classes to init classifier 
        pcaChart.data.datasets = [];
        var allSpecs = [];
        var allSpecsClassIDs = [];
        var promises = [];
        for(classID in this.$data.checkedClasses){
          if(this.checkedClasses[classID] == true){
            if(this.fetchedClasses[classID] == false){
              promises.push(fetchClassSpectrums(classID, continuation));
            }
            else{continuation(classID)}
            function continuation(classID){ //this is called N times, N == checkedClasses.length
              var thisFrame = frame.$data;
              var allSpecs = thisFrame.allSpecs;
              var allSpecsClassIDs = thisFrame.allSpecsClassIDs;
              for(var x = 0; x < thisFrame.classSpectrums[classID].length; x++){
                var spectrum_data = thisFrame.classSpectrums[classID][x];
                // white correction if enabled
                if(thisFrame.whiteCorrectionEnabled == true){
                  var whiteRef = thisFrame.classSpectrums['v8-ref'];
                  whiteRef = normalizeSpectrum(whiteRef[0]);
                  var whiteCorrected = whiteCorrect(spectrum_data, whiteRef);
                  spectrum_data = whiteCorrected;
                }
                if(thisFrame.normalizationEnabled == true){
                  allSpecs.push(normalizeSpectrum(spectrum_data));
                }
                else{
                  allSpecs.push(spectrum_data);
                }
                allSpecsClassIDs.push(classID);
              }
              console.log("Classifier Init")
            }// end continuation
          }
        }
        // after all is fetched (multiple async calls)
        Promise.all(promises).then(function(b) {
          console.log("listening")
          var unclassifiedRef = new Firebase("https://verivina.firebaseio.com/unclassified/?/measurements/single/");
          var spectrum_data;
          unclassifiedRef.on("child_changed", function(childSnapshot, prevChildKey){
            console.log("child changed");
            if(childSnapshot.key() == "spectrum"){
              var spectrum = childSnapshot.val();
              
              spectrum_data = formatSpectrum(spectrum, false);          
              
              // do PCA and KNN
              if(frame.$data.normalizationEnabled == true){
                spectrum_data = normalizeSpectrum(spectrum_data);
              }
              frame.$data.allSpecs.push(spectrum_data);
              //frame.$data.allSpecsClassIDs.push("unclassified");
              //frame.$data.classColors["unclassified"] = "#000"
              var XR = pcaReduce(pca(frame.$data.allSpecs) , 2); // 2 loadings
              frame.$data.allSpecs.pop();
              frame.$data.knearMachine = new kNear(5);    // knn injection 
              for(var i = 0; i < frame.$data.allSpecs.length; i++){
                var pcaPoint = pcaProject(frame.$data.allSpecs[i], XR);
                frame.$data.knearMachine.learn([pcaPoint[0],pcaPoint[1]], frame.$data.allSpecsClassIDs[i]);
              }
              var classifier_guess = frame.$data.knearMachine.classify(pcaProject(spectrum_data, XR));
              console.log("The guess is: " + classifier_guess);
              frame.$data.classifierGuess = classifier_guess;
            }
          }); // end on("child_changed")

        }, function(err) {
            // error occurred
        });
      },// end InitClassifier

      SelectClass: 
      function(classID, event){
        if(this.fetchedClasses[classID] == false){
          fetchClassSpectrums(classID, continuation);
        }
        else{continuation(classID)}
        function continuation(classID){
          for(i in frame.$data.classSpectrums[classID]){
            var spectrum_data = frame.$data.classSpectrums[classID][i];
            // white correction if enabled
            if(frame.$data.whiteCorrectionEnabled == true){
              var whiteRef = frame.$data.classSpectrums['v7ref-ss53'];
              whiteRef = normalizeSpectrum(whiteRef[0]);
              var whiteCorrected = whiteCorrect(spectrum_data, whiteRef);
              spectrum_data = whiteCorrected;
            }
            if(frame.$data.normalizationEnabled == true){
              spectrum_data = normalizeSpectrum(spectrum_data);
            }
            var d = {
              labels: range(400,750), 
              label: classID ,
              data: spectrum_data,
              fill: false,
              lineTension: 0.1,
              backgroundColor: frame.$data.classColors[classID],
              borderColor: frame.$data.classColors[classID],
              borderCapStyle: 'butt',
              borderDash: [],
              borderDashOffset: 0.0,
              borderJoinStyle: 'miter',
              pointBorderColor: "rgba(75,192,192,1)",
              pointBackgroundColor: "#fff",
              pointBorderWidth: 1,
              pointHoverRadius: 5,
              pointHoverBackgroundColor: "rgba(75,192,192,1)",
              pointHoverBorderColor: "rgba(220,220,220,1)",
              pointHoverBorderWidth: 2,
              pointRadius: 0,
              pointHitRadius: 10,
              spanGaps: false
            };
            myChart.data.datasets.push(d);
            myChart.update();
          }
        }
      },
      PerformPCA: 
        function(event){
          pcaChart.data.datasets = [];
          var allSpecs = [];
          var allSpecsClassIDs = [];
          var promises = [];
          for(classID in this.$data.checkedClasses){
            if(this.checkedClasses[classID] == true){
              if(this.fetchedClasses[classID] == false){
                promises.push(fetchClassSpectrums(classID, continuation));
              }
              else{continuation(classID)} 
            }
          }
          // continuation function for fetch to fill allSpecs, after being fetched
          function continuation(classID){ 
            var thisFrame = frame.$data;
            for(var x = 0; x < thisFrame.classSpectrums[classID].length; x++){
              var spectrum_data = thisFrame.classSpectrums[classID][x];
              // white correction if enabled
              if(thisFrame.whiteCorrectionEnabled == true){
                var whiteRef = thisFrame.classSpectrums['v8-ref'];
                whiteRef = normalizeSpectrum(whiteRef[0]);
                var whiteCorrected = whiteCorrect(spectrum_data, whiteRef);
                spectrum_data = whiteCorrected;
              }
              if(thisFrame.normalizationEnabled == true){
                allSpecs.push(normalizeSpectrum(spectrum_data));
              }
              else{
                allSpecs.push(spectrum_data);
              }
              allSpecsClassIDs.push(classID);
            }
          }//end of continuation()

          Promise.all(promises).then(function(b) {
              plotPCA(allSpecs, allSpecsClassIDs);
          }, function(err) {
              // error occurred
          });

        }, // end PerformPCA,

      DeleteDataSet: function(event) {
        var specDBRef = new Firebase("https://verivina.firebaseio.com/spec_db");
         for(classID in this.checkedClasses){
          if(this.checkedClasses[classID] == true){
            specDBRef.child(classID).set({});
            alert(classID + " deleted!");
          }
        }


      }, // end DeleteDataSet()

      ClearGraph: function(event) {
        pcaChart.data.datasets = [];
        pcaChart.update();

        myChart.data.datasets = [];
        myChart.update();
      }
      

    }
})


// END INIT VUE.JS
